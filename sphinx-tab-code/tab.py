from docutils.parsers.rst.directives.admonitions import Directive
from docutils.parsers.rst import states, directives
from docutils.parsers.rst.roles import set_classes
from docutils import nodes

class tab_node(nodes.Element):
    """The base 'tab' node."""

class Tab(Directive):

    required_arguments = 1
    final_argument_whitespace = True
    option_spec = {'class': directives.class_option,
                   'name': directives.unchanged}
    has_content = True

    def run(self):
        set_classes(self.options)
        self.assert_has_content()
        text = '\n'.join(self.content)
        base_node = tab_node(text, **self.options)
        self.state.nested_parse(self.content, self.content_offset,
                                base_node)
        return [base_node]

def visit_tab_html(self, node): pass

def depart_tab_html(self, node): pass

def setup(app):
    app.add_node(tab_node, html=(visit_tab_html, depart_tab_html))
    app.add_directive('tab', Tab)
