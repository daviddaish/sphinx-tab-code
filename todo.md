todo:

* Add an argument to act as the name of the tab.
* Add a html and CSS wrapper to visually demonstrate that the content is a tab.
  Make sure the styling is done such that it is portable across styles. Look at
  how the admonitions have done their styling.
* Add javascript to work the tabs. Assure the document's tab logic works without
  CSS, and has no dependencies.
* Look into what extra options, and Directive/Element methods would be
  worthwhile to include.
* Write CSS for the catalyst cloud style, and include demo/example CSS for
  people coming along.
* Package, and test the code on the Catalyst Cloud docs
* Distribute and advertise it to the OS community.
